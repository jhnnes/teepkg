#!/bin/sh

# TeePKG 1.2
# Author: Raven
# License: 3-Clause BSD

#############################
### package manager check ###
#############################

# debian, ubuntu
if [ -f /usr/bin/apt ]; then
	pkgmngr="apt"
# fedora, rhel, almalinux
elif [ -f /usr/bin/dnf ]; then
	pkgmngr="dnf"
# archlinux
elif [ -f /usr/bin/pacman ]; then
	pkgmngr="pacman"
# opensuse, suse
elif [ -f /usr/bin/zypper ]; then
	pkgmngr="zypper"
# gentoo
elif [ -f /usr/bin/emerge ]; then
	pkgmngr="emerge"
# void linux
elif [ -f /usr/bin/xbps-install ]; then
	pkgmngr="xbps"
# old rhel and rhel-based distros
elif [ ! -f /usr/bin/dnf ] && [ -f /usr/bin/yum ]; then
	pkgmngr="yum"
# solus
elif [ -f /usr/bin/eopkg ]; then
	pkgmngr="eopkg"
# freebsd
elif [ -f /usr/local/bin/pkg ]; then
	pkgmngr="pkg"
# openbsd
elif [ -f /usr/sbin/pkg_add ]; then
	pkgmngr="pkg_add"
# nixos
elif [ -f /usr/bin/nix-env ]; then
	pkgmngr="nix-env"
fi

####################
### main program ###
####################

teepkg_install(){
	if [ $pkgmngr = "apt" ]; then
		apt install $package
	elif [ $pkgmngr = "dnf" ]; then
		dnf install $package
	elif [ $pkgmngr = "pacman" ]; then
		pacman -S $package
	elif [ $pkgmngr = "zypper" ]; then
		zypper in $package
	elif [ $pkgmngr = "emerge" ]; then
		emerge -aq $package
	elif [ $pkgmngr = "xbps" ]; then
		xbps-install -S $package
	elif [ $pkgmngr = "yum" ]; then
		yum install $package
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg install $package
	elif [ $pkgmngr = "pkg" ]; then
		pkg install $package
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_add $package
	elif [ $pkgmngr = "nix-env" ]; then
		nix-env -iA $package
	fi
}

teepkg_search(){
	if [ $pkgmngr = "apt" ]; then
		apt search $package
	elif [ $pkgmngr = "dnf" ]; then
		dnf search $package
	elif [ $pkgmngr = "pacman" ]; then
		pacman -Ss $package
	elif [ $pkgmngr = "zypper" ]; then
		zypper se $package
	elif [ $pkgmngr = "emerge" ]; then
		emerge --search $package
	elif [ $pkgmngr = "xbps" ]; then
		xbps-query -Rs $package
	elif [ $pkgmngr = "yum" ]; then
		yum search $package
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg search $package
	elif [ $pkgmngr = "pkg" ]; then
		pkg search $package
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_info -Q $package
	elif [ $pkgmngr = "nix-env" ]; then
		nix-env -qaP $package
	fi
}

teepkg_remove(){
	if [ $pkgmngr = "apt" ]; then
		apt remove $package
	elif [ $pkgmngr = "dnf" ]; then
		dnf remove $package
	elif [ $pkgmngr = "pacman" ]; then
		pacman -Rs $package
	elif [ $pkgmngr = "zypper" ]; then
		zypper rm $package
	elif [ $pkgmngr = "emerge" ]; then
		emerge --prune $package
	elif [ $pkgmngr = "xbps" ]; then
		xbps-remove -R $package
	elif [ $pkgmngr = "yum" ]; then
		yum remove $package
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg remove $package
	elif [ $pkgmngr = "pkg" ]; then
		pkg remove $package
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_delete $package
	elif [ $pkgmngr = "nix-env" ]; then
		nix-env -e $package
	fi
}

teepkg_upgrade(){
	if [ $pkgmngr = "apt" ]; then
		apt update
		apt upgrade -y
	elif [ $pkgmngr = "dnf" ]; then
		dnf check-update
		dnf upgrade -y
	elif [ $pkgmngr = "pacman" ]; then
		pacman -Syu
	elif [ $pkgmngr = "zypper" ]; then
		zypper up
	elif [ $pkgmngr = "emerge" ]; then
		emerge --ask --update --quiet --deep --newuse @world
	elif [ $pkgmngr = "xbps" ]; then
		xbps-install -Su
	elif [ $pkgmngr = "yum" ]; then
		yum check-update
		yum upgrade -y
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg update-repo
		eopkg upgrade
	elif [ $pkgmngr = "pkg" ]; then
		pkg update
		pkg upgrade
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_add -u
	elif [ $pkgmngr = "nix-env" ]; then
		nix-env -u
	fi
}

teepkg_clean(){
	if [ $pkgmngr = "apt" ]; then
		apt clean
		apt autoremove
	elif [ $pkgmngr = "dnf" ]; then
		dnf clean all
		dnf autoremove
	elif [ $pkgmngr = "pacman" ]; then
		pacman -Scc
		pacman -Rsn $(pacman -Qdtq)
	elif [ $pkgmngr = "zypper" ]; then
		zypper clean
	elif [ $pkgmngr = "emerge" ]; then
		echo "To avoid the removal of needed packages, I've included the --pretend option."
		emerge --ask --pretend --depclean
	elif [ $pkgmngr = "xbps" ]; then
		xbps-remove -ROo
	elif [ $pkgmngr = "yum" ]; then
		yum clean all
		yum autoremove
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg autoremove
		eopkg clean
	elif [ $pkgmngr = "pkg" ]; then
		pkg autoremove
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_delete -a
	elif [ $pkgmngr = "nix-env" ]; then
		echo "Oh no. I cannot do this :("
	fi
}

teepkg_info(){
	if [ $pkgmngr = "apt" ]; then
		apt show $package
	elif [ $pkgmngr = "dnf" ]; then
		dnf info $package
	elif [ $pkgmngr = "pacman" ]; then
		pacman -Qi $package
	elif [ $pkgmngr = "zypper" ]; then
		zypper info $package
	elif [ $pkgmngr = "emerge" ]; then
		echo "Oh no. I cannot do this :("
	elif [ $pkgmngr = "xbps" ]; then
		xbps-query -S $package
	elif [ $pkgmngr = "yum" ]; then
		yum info $package
	elif [ $pkgmngr = "eopkg" ]; then
		eopkg info $package
	elif [ $pkgmngr = "pkg" ]; then
		pkg info $package
	elif [ $pkgmngr = "pkg_add" ]; then
		pkg_info -d $package
	elif [ $pkgmngr = "nix-env" ]; then
		nix-env -qa --description $package
	fi
}

teepkg_version(){
	echo "TeePKG Version 1.2"
}

teepkg_help(){
	echo ""
	echo "TeePKG 1.2"
	echo ""
	echo "Syntax: teepkg [option] package"
	echo ""
	echo "Options:"
	echo ""
	echo "-i, install"
	echo "Install a package"
	echo
	echo "-s, search"
	echo "Search for a package"
	echo
	echo "-r, remove"
	echo "Remove a package"
	echo
	echo "-u, upgrade"
	echo "Upgrade all packages"
	echo
	echo "-c, clean"
	echo "Clean the system"
	echo
	echo "-a, info"
	echo "Show informations about a package"
	echo
	echo "-v, version"
	echo "Show version"
	echo
	echo "-h, help"
	echo "Show this help"
	echo ""
	echo "For more informations, see teepkg[1]."
	echo ""
}

if [ "$#" != 0 ]; then
	opt=$1
	case "$opt" in
		-s | search)
		shift
		for package in "$*"; do
			teepkg_search
		done
		;;
		-i | install)
		shift
		for package in "$*"; do
			teepkg_install
		done
		;;
		-r | remove)
		shift
		for package in "$*"; do
			teepkg_remove
		done
		;;
		-u | upgrade)
		teepkg_upgrade
		;;
		-c | clean)
		teepkg_clean
		;;
		-a | info)
		shift
		for package in "$*"; do
			teepkg_info
		done
		;;
		-v | version)
		teepkg_version
		;;
		-h | help)
		teepkg_help
		;;
		?)
		echo "Usage: teepkg [option] package"
		exit 1
		;;
	esac
fi
